# -*- coding: utf8 -*-

from distutils.core import setup
from distutils.extension import Extension
import os
import fnmatch


def filter_filenames( filenames, match_list ):
    matched = []
    for name in filenames:
        for p in match_list:
            if fnmatch.fnmatch(name, p):
                matched.append( name )
                break # patterns
    return matched


def find_files(search_root, match_list):
    found = []
    for dirpath, dirnames, filenames in os.walk(search_root, followlinks = True):
        for d in dirnames[:]:
            if d.startswith("."):
                dirnames.remove(d)
        cur_dir_abs = os.path.abspath(dirpath)
        filenames_abs = []
        for name in filenames:
            if os.path.isabs(name):
                filenames_abs.append(name)
            else:
                filenames_abs.append(os.path.join(cur_dir_abs, name))
        found += filter_filenames(filenames_abs, match_list)
    return found


extra = {}
have_cython = False
try:
    from Cython.Distutils import build_ext as _build_ext
    have_cython = True
    extra = dict(cmdclass={'build_ext': _build_ext})
except ImportError:
    pass

SRC_PATH = 'src'

def src_path(*args):
    return [os.path.join(SRC_PATH,x) for x in args ]

if have_cython:
    pyurl  = Extension('pyurl',
                        src_path('pyurl.pyx') +
                        src_path('qs_parse.c') +
                        src_path('url.c')
                       )
else:
    pyurl  = Extension('pyurl',
                        src_path('pyurl.c') +
                        src_path('qs_parse.c') +
                        src_path('url.c')
                       )


setup (
        name='pyurl',
        version='0.1.4',
        packages= [],
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description='python url manipulation library',
        long_description="""python url manipulation library""",
        license = 'bsd',
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                              }},
        classifiers = ['misc'],
        ext_modules=[pyurl],
        #cmdclass={'build_ext': _build_ext}
        **extra
        )



