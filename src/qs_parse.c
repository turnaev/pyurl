
#include "qs_parse.h"

int extract_qs_kv(const char* spec,
                            component_t* query,
                            component_t* key,
                            component_t* value) {
  if (query->len <= 0)
    return 0;

  int start = query->begin;
  int cur = start;
  int end = query->begin + query->len;

  // We assume the beginning of the input is the beginning of the "key" and we
  // skip to the end of it.
  key->begin = cur;
  while (cur < end && spec[cur] != '&' && spec[cur] != '=')
    cur++;
  key->len = cur - key->begin;

  // Skip the separator after the key (if any).
  if (cur < end && spec[cur] == '=')
    cur++;

  // Find the value part.
  value->begin = cur;
  while (cur < end && spec[cur] != '&')
    cur++;
  value->len = cur - value->begin;

  // Finally skip the next separator if any
  if (cur < end && spec[cur] == '&')
    cur++;

  // Save the new query
  //*query = MakeRange(cur, end);
  query->len = end - cur;
  query->begin = cur;
  return 1;
}



