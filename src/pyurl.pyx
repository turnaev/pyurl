

from cpython.string cimport PyString_GET_SIZE, PyString_FromStringAndSize, PyString_Check, PyString_AS_STRING

from libc.stdlib cimport malloc, free
from libc.string cimport memcpy

DEF MAX_STACK_STR = 1024

cdef extern from "url.h":
    void url_unescape(char *s, int py_url)

cdef extern from "qs_parse.h":
    struct component:
        int begin
        int len

    int extract_qs_kv(char* spec, component * query, component * key, component * value)


def parse_qs(qs, int unquote = 0, int plus = 1):
    cdef:
        object result
        char * c_str_qs
        int qs_len
        component query_string
        component qs_key
        component qs_value

    if not PyString_Check(qs):
        raise TypeError("expected string got: %s instead." % type(qs))

    result = {}
    c_str_qs = PyString_AS_STRING(qs)
    query_string.begin = 0
    query_string.len = PyString_GET_SIZE(qs)
    while extract_qs_kv(c_str_qs, &query_string, &qs_key, &qs_value):
        key = PyString_FromStringAndSize(c_str_qs + qs_key.begin, qs_key.len)
        if unquote:
            value = _unescape(c_str_qs + qs_value.begin, qs_value.len, plus)
        else:
            value = PyString_FromStringAndSize(c_str_qs + qs_value.begin, qs_value.len)
        result[key] = value
    return result

cdef inline object _unescape(char * url_str, int url_len, int plus):
    cdef:
        char * tmp
        object result
        char tmp_arr[MAX_STACK_STR]

    if url_len <= 0:
        return ""

    if url_len < MAX_STACK_STR:
        tmp = tmp_arr
    else:
        tmp = <char *>malloc(url_len + 1)
        if tmp == NULL:
            raise MemoryError("url_unescape: tmp = malloc(url_len)")
    try:
        tmp = <char *>memcpy(tmp, url_str, url_len)
        tmp[url_len] = 0x00
        url_unescape(tmp, plus)
        result = tmp
    finally:
        if url_len >= MAX_STACK_STR:
            free(tmp)
    return result

def unescape(url_str, int plus = 1):
    if not PyString_Check(url_str):
        raise TypeError("url_unescape: parameter must be string got: %s instead." % type(url_str))
    return _unescape(PyString_AS_STRING(url_str), PyString_GET_SIZE(url_str), plus)



