
#ifndef QS_PARSE
#define QS_PARSE

#include <string.h>

// Represents a substring for URL parsing.
typedef struct component {
  int begin;  // Byte offset in the string of this component.
  int len;    // Will be -1 if the component is unspecified.
} component_t;


int extract_qs_kv(const char* spec,
                  component_t* query,
                  component_t* key,
                  component_t* value);

#endif


