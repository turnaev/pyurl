
#ifndef URL_H
#define URL_H

#include <stdbool.h>
#include <ctype.h>

#define XDIGIT_TO_NUM(h) ((h) < 'A' ? (h) - '0' : toupper (h) - 'A' + 10)
#define X2DIGITS_TO_NUM(h1, h2) ((XDIGIT_TO_NUM (h1) << 4) + XDIGIT_TO_NUM (h2))

void url_unescape (char *s, int plus);

#endif

